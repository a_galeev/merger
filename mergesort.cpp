#include "mergesort.h"
#include "checker.h"

template<typename T>    //��� �������� ������������ �� ������
bool isSorted(vector<T>& _arr)
{
    if(_arr.size() == 1)
        return true;
    for(int i = 1; i < _arr.size(); ++i)
        if(_arr[i-1] > _arr[i])
            return false;
    return true;
}

template<>
bool comp<int>(const int& vl, const int& vr){ return (vl < vr); }//������� ���������, ��� ���������� �� �����������

template<>
bool comp<string>(const string& vl, const string& vr)//���������� � ��� ��������� �������, �� � ���� ������� ������ ������ ������ ����� �������
{
    if(vl.size() == 0)
        return false;
    if(vr.size() == 0)
        return true;
    return (vl < vr);
}


template<typename T>
void mergeSubArr(vector<T>& _arr, int l, int m, int r)
{
    int i = l;
    int j = m + 1;
    int c = 1 + r - l;
    if(c <= 1)
        return;
    vector<T> v_tmp(c);

    for(int it = 0; it < c; ++it)
    {
        if((i <= m) && (j <= r))
        {
            if(_arr[i] < _arr[j])
                v_tmp[it] = _arr[i++];
            else
                v_tmp[it] = _arr[j++];
        }
        else
        {
            if(i <= m)
                v_tmp[it] = _arr[i++];
            else
                v_tmp[it] = _arr[j++];
        }
    }
    i = 0;
    for(int it = l; it <= r; ++it)
        _arr[it] = v_tmp[i++];

}

template<typename T>    //����������� ������� ���������� ��������
void mergeSort(vector<T>& _arr, int l, int r)
{
    if((l < r) && (!isSorted(_arr)))    //��� ������� ������, ��� ��� �������� �������� ��������, ��� ������� ���������� ���������� ������ ���
    {                                   //�� ���������������� �������, ������� �� �������� ��� ��������� ����� (����� ���� �������� ������� ��� ������� ������, ��� �� �������������� ������ �������� � ����� �������
        int m = (r - l) / 2 + l;
        mergeSort(_arr, l, m);
        mergeSort(_arr, m + 1, r);
        mergeSubArr(_arr, l ,m, r);
    }
}

template void mergeSort<int>(vector<int>& _arr, int l, int r);
template void mergeSort<string>(vector<string>& _arr, int l, int r);


template<typename T>//��������� ������ �� ��������� ������ � ������� ��������� � outputStream
void mergeFiles(ostream& outF, vector<pair <string, int> >& vp)
{
    vector<ifstream*> v_ifs;
    vector<T> data;
    string tmp;
    T res;

    for(pair <string, int>& el : vp)
    {
        ifstream* ifs = new ifstream(el.first);// ��� ������ ����� ������� ��� ����� � ������� ������
        //ifs.open(el.first);
        if(!ifs)
            throw runtime_error("Could not open temp data!");//��� �����, ���� ��� �� �����������, ��� � ������ ����� ����� ��������� ���������
        if(el.second > 0)
        {
            v_ifs.push_back(ifs);//��������� � ������ ������� �����, ���� � ��� ���� ����-�� ���� ���� � �������
            getline(*ifs, tmp);
            if(check_data<T>(tmp, res))
                data.push_back(res);//����� ����� ������ ����, ��� ��� ������ � ������ �������������, �� ��� ����� ����������� �������
        }

        else
            throw runtime_error("Temp data were corrupted!");
    }


    //getline(*v_ifs[0], tmp);


    int min_el_indx;
    while(1)//���� �� �������� ��� ������� �������
    {

        min_el_indx = min_element(data.begin(),data.end(), comp<T>) - data.begin();//���� ������� ������������ ���� ������

        if(data[min_el_indx] == fin<T>()) // ���� �������� ���� ������ - ��� ���� ����������, �� ������ �� �����
            break;
        outF<<data[min_el_indx]<<endl;  //����� ���������� ����������� ������� � �������� �����
        if(vp[min_el_indx].second > 0)  //���� � �����, �� �������� �� ��������� ����������� �������, ��� ���-�� ����, �� �������������� �������� �� ���������
        {
            vp[min_el_indx].second--;
            getline(*v_ifs[min_el_indx], tmp);

            if(!check_data<T>(tmp, res))    //�������� �� ������ ������
            {
                data[min_el_indx] = fin<T>();
                (*v_ifs[min_el_indx]).close();
                remove((vp[min_el_indx].first).c_str());

            }
            else
                data[min_el_indx] = res;
        }
        else    //���� �� ��� ������ ������, �� ��������� ����� � �����, ������� �����
        {
            data[min_el_indx] = fin<T>();
            (*v_ifs[min_el_indx]).close();
            if(remove((vp[min_el_indx].first).c_str()) != 0)
                cerr<<"Failed to delete temporary file. You can remove it manually."<<endl;
        }
    }


    for (int i =0; i< v_ifs.size();i++) //����������� ������
    {

        delete (v_ifs[i]);
    }
    v_ifs.clear();
}
template void mergeFiles<int>(ostream& outF, vector<pair <string, int> >& vp);
template void mergeFiles<string>(ostream& outF, vector<pair <string, int> >& vp);
