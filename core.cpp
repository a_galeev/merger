#include "core.h"
#include "mergesort.h"
#include "checker.h"



template <typename T>   //�������� ���� ���������
void run(vector<string>& if_arr, string& outFile)//��������� �������� ������� ������ � ���� ������ ��������
{
    vector<pair <string, int> > v = splitFile<T>(if_arr);   //��������� ������� ������ �� ������� ��������
    ofstream fout(outFile);
    ostream* os = &cout;//������� �������� �����, ���� ����� �������� ���� �� ���������, �� ���� �� � ������� ������� ����������
    if(!fout)
    {
        cerr<<"Could not open output file "<<outFile<<endl;
        cerr<<"Merge results were shown in the console"<<endl;
    }
    else
        os = &fout;

    mergeFiles<T>(*os, v);//��������� ���������� �������� ������������� ������

}
template void run<int>(vector<string>& if_arr, string& outFile);
template void run<string>(vector<string>& if_arr, string& outFile);
