#include "checker.h"


//������, ������������ ��� � ������ ������ ��� ������
template<>
int fin<int>(){return std::numeric_limits<int>::max();}
template<>
string fin<string>(){string s(""); return s;}

template<>  //������� �������� ������ ��� int: ���������� 1 ���� ����� �������������� string->int � ���������� ��������� � result, ����� false
bool check_data<int>(const string& str, int& result)    //string to int converter
{                                           //return true on success, otherwise false
    try
    {
        size_t lastChar;
        result = stoi(str, &lastChar, DEC);
        if(result == fin<int>())
            throw std::out_of_range("int data type out of range");
        return lastChar == str.size();
    }
    catch (std::invalid_argument&)
    {
        return false;
    }
    catch (std::out_of_range&)
    {
        return false;
    }
}

template<>//������ �������, ���� ��� �� �����
bool check_data<string>(const string& str, string& result)
{
    if((str.empty()))
        return false;
    result = str;
    return true;
}


