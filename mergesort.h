#pragma once
#include<vector>
#include <iostream>
#include <fstream>
#include <iterator>
#include <algorithm>
#include <ostream>
using namespace std;

//��� ��� ���������� ��������

template<typename T>
extern bool isSorted(vector<T>& _arr);

template<typename T>
extern bool comp(const T& vl, const T& vr);


template<typename T>
extern void mergeSubArr(vector<T>& _arr, int l, int m, int r);

template<typename T>
extern void mergeSort(vector<T>& _arr, int l, int r);

template<typename T>
extern void mergeFiles(ostream&, vector<pair <string, int> >& );
