#include "mergesort.h"
#include "my_constants.h"
#include "splitter.h"
#include "core.h"

using namespace std;

int main(int argc, char* argv[])
{

    try
    {
        if(argc < min_num_of_args)  //�������� �� ����������� ���������� ����������
            throw runtime_error("Too few arguments in cmd");

        string outFile = string(argv[OUT_FILE]);
        vector<string> if_arr;  //������ ���� ��� ������� ������
        for(int i = INP_FILE; i < argc; ++i)
        {
            bool isUniq = true;
            string f = string(argv[i]);
            for(string& el : if_arr)
                if(el == f)
                    isUniq = false;
            if(isUniq)
                if_arr.push_back(f);
            else
                cerr<<"Duplicated file "<< f<<" will be ignored"<<endl;
        }

        string data_type = argv[DATA_TYPE];
        if(data_type == "-i")   //�������� ��� �����
            run<int>(if_arr, outFile);
        else if(data_type == "-s")  //�������� ��� �����
            run<string>(if_arr, outFile);
        else
            throw runtime_error("Unknown data type flag(-i for integer or -s for string is required.");
    }

    catch(runtime_error& e)
    {
        cerr<<e.what()<<endl;
    }
    cout<<"Done. See output file with result"<<endl;

    return 0;
}
