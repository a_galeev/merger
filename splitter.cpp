#include "splitter.h"
#include "checker.h"


template<typename T>    //������� ��� ��������� �������� ����� �� ���������, ����� ����� ���� ��������� �� �� ������ � ������
vector<pair <string, int> > splitFile(vector<string>& _inFiles)
{
    string tmp;
    int res;
    int counter = 0;//��� ����� ���������� �����
    vector<pair <string, int> > v_res;//�.�. �� ���������� ����<��� ���������� �����, ���������� ������� � ���>
    //vector<string> ofArray;
    for(string& file : _inFiles)
    {
        ifstream ifs(file); //��������� ���� ������� �����
        if(ifs)
        {
            while(!ifs.eof())
            {
                string ofName = to_string(counter);
                ofstream ofs(ofName);   //������� ��������� �������� ���� (��-��������, ��� ����� ������ ��������)
                vector<T> arr;  //������ ��� ������ �� �������� �����
                for(int i = 0; i < max_num_of_elems && !ifs.eof();)
                {

                    getline(ifs>>std::ws, tmp);//������ ������� �� �������� �����, ����������� �� �����������


                    T res;
                    if(check_data<T>(tmp, res))//��������� ����������� ������
                    {

                        arr.push_back(res);
                        ++i;
                    }

                    else if(!tmp.empty())//���� ������ �� ������� (��������, ���������� ������������� � int), ����������
                    {
                        cerr<<"Skipped invalid value from file " + file<<endl;


                    }

                }
                mergeSort(arr, 0, arr.size() - 1);  //� ������ ������ �������� ��������� ������
                for(T& el : arr)
                {
                     ofs<<el<<endl; //� ��������� ������ �� ��������� ����
                }
                //if(!isSorted(_arr))
                //splitFile sf(ofName, arr.size());
                //lf.push_back(sf);
                v_res.push_back(make_pair(ofName, arr.size())); //��������� ��� ���������� ����� � ����� ��������� ���
                //ofArray.push_back(ofName);

                counter++;//��� �������� ��������� �����
                ofs.close();

            }//��������� ������
            ifs.close();
        }
        else
            cerr<<file<<" file read error!"<<endl;//���� �� ����������� ������� ����, �� ����������
    }

    return v_res;
}

template vector<pair <string, int> > splitFile<int>(vector<string>& _inFiles);
template vector<pair <string, int> > splitFile<string>(vector<string>& _inFiles);
